from sklearn import svm
from sklearn import metrics


# Performs training and testing of any "sk_learn" classifier inserted
# using the model attribute. Also performs validation on the fraction
# of the training data.
class SkLearnClassifier:

    # Model is any valid sk_learn classifier and
    # param refers to custom setting.
    def __init__(self, model, param):
        self.model = model
        self.param = param

    def get_model(self):
        return self.model

    # Fits the model using training data:
    # x[number of examples x number of channels x samples in each epoch x 1]
    # y[number of examples x number of categories - default 2]
    # [xval, yval] - validation data to evaluate
    def fit(self, x, y, xval, yval):
        # reshape to number of examples x number of features if necessary
        if x.ndim == 4:
            x = x.reshape((x.shape[-4], -1))

        self.model.fit(x, y[:, 0])
        return self.evaluate(xval, yval)

    # Evaluates the classifier using testing data:
    # x[number of examples x length of each feature vector]
    # y[number of examples x number of categories - default 2]
    # Returns: [(loss) accuracy precision recall]
    def evaluate(self, x, y):
        predictions = []
        real_outputs = []
        for i in range(x.shape[0]):
            pattern = x[i, :].reshape(1, -1)
            prediction = self.model.predict(pattern)
            predictions.append(prediction[0])
            real_outputs.append(y[i, 0])
        auc = metrics.roc_auc_score(real_outputs, predictions)
        acc = metrics.accuracy_score(real_outputs, predictions)
        prec = metrics.precision_score(real_outputs, predictions)
        recall = metrics.recall_score(real_outputs, predictions)
        if self.param.verbose:
            print("Confusion matrix: ", metrics.confusion_matrix(real_outputs, predictions))
            print("AUC: ", metrics.roc_auc_score(real_outputs, predictions))
        return [auc, acc, prec, recall]
