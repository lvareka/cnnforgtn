FROM python:3.6-slim

WORKDIR /app

COPY ./requirements.txt ./
RUN pip3 install -r requirements.txt

COPY . .
RUN pip3 install .

CMD python3 ./main/run_analysis.py