import numpy as np
from sklearn import preprocessing


# Averages selected time intervals in each out_features[i][j]
# that have been specified using param.intervals.
# Returns 2D feature vectors n_of_examples x (number_of_time_windows x number_of_channels)
def windowed_means(out_features, param):
    # in all feature vectors
    output_features = []
    for i in range(out_features.shape[0]):
        feature = []
        # for all EEG channels
        for j in range(out_features.shape[1]):
            time_course = out_features[i][j]
            for k in range(param.intervals.shape[0]):
                borders = param.intervals[k] * param.sampling_fq
                feature.append(np.average(time_course[int(borders[0] - 1):int(borders[1] - 1)]))
        output_features.append(feature)
#       print(np.shape(output_features))
    return preprocessing.scale(np.array(output_features), axis=1)


# Add a singleton dimension to enable CNN Keras classification
def cnn_reshape(out_features):
    # reshape the data to add a singleton dimension
    out_features = out_features.reshape(out_features.shape[0], out_features.shape[1], out_features.shape[2], 1)
    return out_features


# From out_features, remove all epochs with any channel
# exceeding ampl_threshold in the absolute value.
def reject_amplitude(out_features, out_labels, param):
    # in all feature vectors
    output_features = []
    retain_targets = []
    for i in range(out_features.shape[0]):
        feature = []
        reject = False
        # for all EEG channels
        for j in range(out_features.shape[1]):
            if np.max(np.absolute(out_features[i][j])) > param.rej_threshold:
                reject = True
        if not reject:
            output_features.append(out_features[i])
        retain_targets.append(not reject)
    output_features = np.array(output_features)
    if param.verbose:
        print('Rejected: ', (1 - output_features.shape[0] / out_features.shape[0]) * 100, ' %.')
    return  output_features, out_labels[retain_targets, :]


# Averages every N trials in EEG data structure
# out_features  - EEG feature vectors
# averaging - N - number of trials to average together
def neighbor_average_all(out_features, out_labels, averaging_factor):
    if averaging_factor <= 1:
        return [out_features, out_labels]

    # separate only targets/non-target features
    out_t_features = out_features[out_labels[:, 0] == 1, :]
    out_n_features = out_features[out_labels[:, 1] == 1, :]

    # ensemble average targets and non-targets features
    out__t_features_avg = average(out_t_features, averaging_factor)
    out__n_features_avg = average(out_n_features, averaging_factor)

    # create corresponding labels
    out_t_labels = np.tile(np.array([1, 0]), (out__t_features_avg.shape[0], 1))
    out_n_labels = np.tile(np.array([0, 1]), (out__n_features_avg.shape[0], 1))

    # connect target/non-target features/labels
    out_labels = np.vstack((out_t_labels, out_n_labels))
    out_features = np.concatenate((out__t_features_avg, out__n_features_avg), axis=0)

    return [out_features, out_labels]


# Average features only by a certain factor
def average(out_features, averaging_factor):
    out_eeg_data = []
    for trial in range(0, out_features.shape[0] - 1, averaging_factor):
        avg_fv = np.average(out_features[trial:(trial + averaging_factor), :], axis=0)
        out_eeg_data.append(avg_fv)
    return np.array(out_eeg_data)