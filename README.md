# CNNforGTN

# Configuration
Configuration of parameters can be performed in param.py from the *main* package. For example:

* input_file - can be downloaded from the following public dataset: Mouček, Roman; Vařeka, Lukáš; Prokop, Tomáš; Štěbeták, Jan; Brůha, Petr, 2019, "Replication Data for: Evaluation of convolutional neural networks using a large multi-subject P300 dataset", https://doi.org/10.7910/DVN/G9RRLN, Harvard Dataverse, V1 
* show_plots, verbose - controls outputs presented to the user during execution
* n_epochs - number of epochs for classifier training
* validation - proportion of examples used for (cross-)validation
* val_iter - number of cross-validation iterations 

CNN configuration is to be performed using Keras syntax in cnn.py from the *models* package. 

# Running the classification workflow
The workflow is started using `run_analysis.py` from the *main* package. The classification metrics are printed at the end of the execution.

1. Use Python 3.6.
2. Install dependencies using `pip3 install -r requirements.txt`
3. Install this Python package using `pip3 install .` in the root directory of this project.
4. Make sure the datafile `VarekaGTNEpochs.mat` is available in `data` subdirectory.
5. Run `python3 ./main/run_analysis.py`.

Alternatively, use Docker (requires installation of `docker` and `docker-compose`):

1. `docker-compose build`
2. `docker-compose up`