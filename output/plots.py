import matplotlib.pyplot as plt
import numpy as np


# Displays the course of training
def display_history(hist):
    
    # summarize history for accuracy
    plt.plot(hist.history['binary_accuracy'])
    plt.plot(hist.history['val_binary_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.plot(hist.history['loss'])
    plt.plot(hist.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()


# For verifying validity of the data,
# visual inspection of averaged
# target vs non-target epochs is used.
def plot_average(out_features, out_labels, param):
#   out_features = out_features(out)
    plt.rc('xtick', labelsize=14)
    plt.rc('ytick', labelsize=14)
    plt.rc('grid',  linestyle="--", color='gray')

    average_t = out_features[out_labels[:, 0] == 1, :, :].mean(axis=0)
    average_n = out_features[out_labels[:, 0] == 0, :, :].mean(axis=0)

    base_line = int(round(param.pre_epoch * param.sampling_fq))
    plt.grid(True)
    plt.plot(range(base_line, average_t.shape[1] + base_line),
             average_t[param.plot_channel, :], label='target')  # plot Pz
    plt.show(block=False)
    plt.plot(range(base_line, average_n.shape[1] + base_line),
             average_n[param.plot_channel, :], label='non-target')  # plot Pz
    plt.xlabel('Time [ms]', fontsize=14)
    plt.ylabel('Voltage [μV]', fontsize=14)
    plt.title('Averaged epochs (the Pz channel)', fontsize=14)
    plt.legend(loc='upper left', fontsize=14)
    plt.show()
