from setuptools import setup

setup(
    name='cnnforgtn',
    version='1.0',
    packages=['main', 'models', 'output', 'validation'],
    url='',
    license='',
    author='Lukas Vareka',
    author_email='lvareka@kiv.zcu.cz',
    description='CNN for GTN'
)